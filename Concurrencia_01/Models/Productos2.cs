﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Concurrencia_01.Models
{
    public class Productos2
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [MaxLength(100)]
        [Required]
        public string Nombre { get; set; }
        [Display(Name="Unidad de medida")]
        [MaxLength(10)]
        [Required]
        public string UnidadMedida { get; set; }
        [Display(Name="En existencia")]
        public int EnExistencia { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}