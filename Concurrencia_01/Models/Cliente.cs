﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Concurrencia_01.Models
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public byte Edad { get; set; }
        public Géneros Género { get; set; }
        public string Ocupación  { get; set; }
        public decimal Salario { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
    public enum Géneros
    {
        Masculino, Femenino
    }
}