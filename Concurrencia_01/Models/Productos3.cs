﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Concurrencia_01.Models
{
    public class Productos3
    {
        public int Folio { get; set; }
        public string NombreProducto { get; set; }
        public string UnidadMedida { get; set; }

    }
}