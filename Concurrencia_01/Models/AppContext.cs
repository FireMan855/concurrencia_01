﻿using Folios_01.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Concurrencia_01.Models
{
    public class AppContext:DbContext
    {
        public AppContext() : base("DefaultConnection")
        {

        }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Productos2> Productos2 { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Cliente>().HasKey(x => x.Id);
            modelBuilder.Entity<Cliente>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsConcurrencyToken(true);
            modelBuilder.Entity<Cliente>().Property(x => x.Nombre).HasMaxLength(100).IsRequired().IsConcurrencyToken();
            modelBuilder.Entity<Cliente>().Property(x => x.Apellido).HasMaxLength(100).IsRequired().IsConcurrencyToken(true);
            modelBuilder.Entity<Cliente>().Property(x => x.Edad).IsRequired();
            modelBuilder.Entity<Cliente>().Property(x => x.Género).IsRequired();
            modelBuilder.Entity<Cliente>().Property(x => x.Ocupación).HasMaxLength(200).IsOptional();
            modelBuilder.Entity<Cliente>().Property(x => x.Salario).HasPrecision(18, 2).IsOptional();
            modelBuilder.Entity<Cliente>().Property(x => x.FechaRegistro).HasColumnName("Fecha de registro");
            modelBuilder.Entity<Producto>().HasKey(x => x.Folio);
            modelBuilder.Entity<Producto>().Property(x => x.NombreProducto).HasColumnName("Nombre del producto").HasMaxLength(50);
            modelBuilder.Entity<Producto>().Property(x => x.UnidadMedida).HasColumnName("Unidad de medida").HasMaxLength(10);
            modelBuilder.Entity<Producto>().Property(x => x.RowVersion).IsRowVersion();
            modelBuilder.Entity<Productos3>().HasKey(x => x.Folio);
            modelBuilder.Entity<Productos3>().Property(x => x.Folio).IsConcurrencyToken();
            modelBuilder.Entity<Productos3>().Property(x => x.NombreProducto).HasColumnName("Nombre del producto").HasMaxLength(100);
            modelBuilder.Entity<Productos3>().Property(x => x.UnidadMedida).HasColumnName("Unidad de medida").HasMaxLength(10);
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<Concurrencia_01.Models.Productos3> Productos3 { get; set; }
    }
}