﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Concurrencia_01.Models;

namespace Concurrencia_01.Controllers
{
    public class Productos3Controller : Controller
    {
        private Models.AppContext db = new Models.AppContext();

        // GET: Productos3
        public ActionResult Index()
        {
            return View(db.Productos3.ToList());
        }

        // GET: Productos3/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos3 productos3 = db.Productos3.Find(id);
            if (productos3 == null)
            {
                return HttpNotFound();
            }
            return View(productos3);
        }

        // GET: Productos3/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Productos3/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Folio,NombreProducto,UnidadMedida")] Productos3 productos3)
        {
            if (ModelState.IsValid)
            {
                byte intento = 0;
                Intenta:
                try
                {
                    productos3.Folio = db.Productos.Count() + 1;
                    //productos3.Folio = db.Productos3.Select(x => x.Folio).OrderByDescending(x=>x).FirstOrDefault() + 1;
                    db.Productos3.Add(productos3);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch(DbUpdateConcurrencyException ex)
                {
                    intento++;
                    if (intento >= 3) {
                        ModelState.AddModelError(string.Empty,"Ha sobrepasado el número de intentos, intente más tarde");
                        return View(productos3);
                    }
                    else
                    {
                        goto Intenta;
                    }
                }
            }

            return View(productos3);
        }

        // GET: Productos3/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos3 productos3 = db.Productos3.Find(id);
            if (productos3 == null)
            {
                return HttpNotFound();
            }
            return View(productos3);
        }

        // POST: Productos3/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Folio,NombreProducto,UnidadMedida")] Productos3 productos3)
        {
            if (ModelState.IsValid)
            {
                byte intento = 0;
                Intenta:
                try
                {
                    Productos3 prodnuevo = db.Productos3.Where(x => x.Folio == productos3.Folio).FirstOrDefault();
                    prodnuevo.NombreProducto = productos3.NombreProducto;
                    prodnuevo.UnidadMedida = productos3.UnidadMedida;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    intento++;
                    if (intento >= 3)
                    {
                        ModelState.AddModelError(string.Empty, "Ha sobrepasado el número de intentos, intente más tarde");
                        return View(productos3);
                    }
                    else
                    {
                        goto Intenta;
                    }
                }
            }
            return View(productos3);
        }

        // GET: Productos3/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos3 productos3 = db.Productos3.Find(id);
            if (productos3 == null)
            {
                return HttpNotFound();
            }
            return View(productos3);
        }

        // POST: Productos3/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            byte intento = 0;
            Intenta:
            try 
                { Productos3 productos3 = db.Productos3.Find(id);
                    db.Productos3.Remove(productos3);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            catch (DbUpdateConcurrencyException ex)
            {
                intento++;
                if (intento >= 3)
                {
                    ModelState.AddModelError(string.Empty, "Ha sobrepasado el número de intentos, intente más tarde");
                    return RedirectToAction("Delete", new { id });
                }
                else
                {
                    goto Intenta;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
