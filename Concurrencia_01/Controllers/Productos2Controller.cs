﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Concurrencia_01.Models;

namespace Concurrencia_01.Controllers
{
    public class Productos2Controller : Controller
    {
        private Models.AppContext db = new Models.AppContext();

        // GET: Productos2
        public ActionResult Index()
        {
            return View(db.Productos2.ToList());
        }

        // GET: Productos2/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos2 productos2 = db.Productos2.Find(id);
            if (productos2 == null)
            {
                return HttpNotFound();
            }
            return View(productos2);
        }

        // GET: Productos2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Productos2/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre,UnidadMedida,EnExistencia,RowVersion")] Productos2 productos2)
        {
            if (ModelState.IsValid)
            {
                db.Productos2.Add(productos2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productos2);
        }

        // GET: Productos2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos2 productos2 = db.Productos2.Find(id);
            if (productos2 == null)
            {
                return HttpNotFound();
            }
            return View(productos2);
        }

        // POST: Productos2/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nombre,UnidadMedida,EnExistencia,RowVersion")] Productos2 productos2)
        {
            if (ModelState.IsValid)
            {
                Productos2 productonuevo = db.Productos2.Find(productos2.ID);
                productonuevo.Nombre = productos2.Nombre;
                productonuevo.UnidadMedida = productos2.UnidadMedida;
                productonuevo.EnExistencia = productos2.EnExistencia;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productos2);
        }

        // GET: Productos2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos2 productos2 = db.Productos2.Find(id);
            if (productos2 == null)
            {
                return HttpNotFound();
            }
            return View(productos2);
        }

        // POST: Productos2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Productos2 productos2 = db.Productos2.Find(id);
            db.Productos2.Remove(productos2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
