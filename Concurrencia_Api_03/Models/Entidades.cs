﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Concurrencia_Api_03.Models
{
    public class Entidades
    {
    }
    public interface IIdentificable
    {
        int Folio { get; set; }
        string Nombre { get; set; }
    }
    public class Cuenta : IIdentificable
    {
        public int Folio { get; set; }
        public string Nombre { get; set; }
        public decimal Saldo { get; set; }

    }
    public class Reporte : IIdentificable
    {
        public int Folio { get; set; }
        public string Nombre { get; set; }
        public decimal Monto { get; set; }
        public DateTime Fecha { get; set; }
    }
    public class AppContext: DbContext
    {
        public AppContext() : base("DefaultConnection")
        {

        }
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Reporte> Reportes { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelbuilder)
        {
            modelbuilder.Entity<Cuenta>().HasKey(x => x.Folio);
            modelbuilder.Entity<Cuenta>().Property(x => x.Folio).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            modelbuilder.Entity<Cuenta>().Property(x => x.Nombre).HasMaxLength(100);
            modelbuilder.Entity<Cuenta>().Property(x => x.Saldo).HasPrecision(18, 2);
            modelbuilder.Entity<Reporte>().HasKey(x => x.Folio);
            modelbuilder.Entity<Reporte>().Property(x => x.Nombre).HasMaxLength(100);
            modelbuilder.Entity<Reporte>().Property(x => x.Monto).HasPrecision(18, 2);
            modelbuilder.Entity<Reporte>().Property(x => x.Fecha);
        }
    }
}