﻿using Concurrencia_Api_03.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace Concurrencia_Api_03.Controllers
{
    public class CuentaController : ApiController
    {
        private static object ThisLock = new object();
        public Models.AppContext db = new Models.AppContext();

        [HttpGet]
        public IQueryable<Cuenta> GetAll()
        {
            return db.Cuentas.AsQueryable();
        }
        [HttpGet]
        public IHttpActionResult GetOne(int? id)
        {
            if (id != null)
            {
                var cuenta = db.Cuentas.Find(id);
                if (cuenta != null)
                {
                    return Ok(db.Cuentas.Find(id));
                }
                return NotFound();
                
            }
            return BadRequest("No ha especificado una ID");
        }
        [HttpPost]
        public IHttpActionResult Create(string Nombre, decimal Saldo)
        {
            try
            {
                Monitor.Enter(ThisLock);
                Cuenta account = new Cuenta();
                account.Nombre = Nombre;
                account.Saldo = Saldo;
                db.Cuentas.Add(account);
                IHttpActionResult respuesta =db.SaveChanges() > 0 ? Ok("Cuenta creada") : (IHttpActionResult)InternalServerError();
                return respuesta;
            }
            finally
            {
                Monitor.Exit(ThisLock);
            }

        }
        [HttpPut]
        public IHttpActionResult Edit(Cuenta account, string Persona)
        {
            try
            {
                if (string.IsNullOrEmpty(Persona))
                {
                    return BadRequest("Debe colocar su nombre como la persona que editó");
                }
                Monitor.Enter(ThisLock);
                Cuenta account2 = db.Cuentas.Find(account.Folio);
                account2.Nombre = account.Nombre;
                account2.Saldo += account.Saldo;
                Reporte report = new Reporte();
                report.Folio = db.Reportes.Count() + 1;
                report.Nombre = Persona;
                report.Monto = account.Saldo;
                report.Fecha = DateTime.Now;
                db.Reportes.Add(report);
                IHttpActionResult respuesta = db.SaveChanges() > 0 ? Ok("Edición existosa") : (IHttpActionResult)InternalServerError();
                return respuesta;
            }
            finally
            {
                Monitor.Exit(ThisLock);
            }
        }

    }
}
