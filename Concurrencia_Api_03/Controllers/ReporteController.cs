﻿using Concurrencia_Api_03.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Concurrencia_Api_03.Controllers
{
    public class ReporteController : ApiController
    {
        [HttpGet]
        public IQueryable<Reporte> GetAll()
        {
            using (var context = new Models.AppContext())
            {
                return context.Reportes.AsQueryable();
            }
        }
    }
}
