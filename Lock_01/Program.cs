﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Lock_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            Thread[] threads = new Thread[15];
            Departamento dep = new Departamento(1000);
            for (int i = 0; i < threads.Length; i++)
            {
                Thread t = new Thread(new ThreadStart(dep.transacciones));
                threads[i] = t;
            }
            foreach (var hilo in threads)
                hilo.Start();
            Console.ReadKey();
        }
    }
   
    class Departamento
    {
        private object thislock = new object();
        int salario;
        Random r = new Random();
        public Departamento(int inicial) => salario = inicial;

        [MethodImpl(MethodImplOptions.Synchronized)]
        void retiro(int monto)
        {
            //lock (thislock){
            try
            {
                //Monitor.Enter(thislock);
                if (salario >= monto)
                {
                    Console.WriteLine($"Salario antes de retirar: {salario}");
                    Console.WriteLine($"Monto a retirar: {monto}");
                    salario -= monto;
                    Console.WriteLine($"Salario después de retirar: {salario}");
                    Console.WriteLine("--------------------------------------");
                }
                else
                {
                    Console.WriteLine($"No hay fondos suficientes");
                    Console.WriteLine("--------------------------------------");
                }
            }
            finally
            {
               //Monitor.Exit(thislock);
            }
            //}
        }
        public void transacciones()
        {
            retiro(r.Next(1, 100));
        }
    }
}
