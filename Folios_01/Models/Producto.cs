﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Folios_01.Models
{
    public class Producto
    {
        public string Folio { get; set; }
        public string NombreProducto { get; set; }
        public string UnidadMedida { get; set; }
        public byte[] RowVersion { get; set; }
    }
}